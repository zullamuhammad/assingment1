import React, { Component } from 'react'
import { ScrollView, Text, View, StyleSheet, TextInput, Image } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Icon2 from 'react-native-vector-icons/FontAwesome5'

export class Assingment1 extends Component {
    constructor() {
        super()
        this.state = {
            data: []
        }
    }

    componentDidMount = () => {
        fetch('http://www.omdbapi.com/?s=avengers&apikey=997061b4&')
            .then(response => response.json())
            .then(jjson => this.setState({ data: jjson.Search }))
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={{ color: 'white', fontSize: 20 }}>Playing Now</Text>
                    <View style={styles.search}>
                        <Icon name='search' size={20} style={{ color: 'white' }} />
                        <TextInput style={{ color: 'white' }} placeholder='Search' placeholderTextColor='white' />
                    </View>
                </View>
                <ScrollView>
                    {this.state.data.map((value, index) => (
                        <View key={index} style={styles.box}>
                            <Image source={{ uri: value.Poster }} style={styles.poster} />
                            <View style={{ flex: 1, justifyContent: 'space-between', padding: 10 }}>
                                <Text style={{ color: 'white', fontSize: 20 }}>{value.Title}</Text>
                                <Text style={{ color: '#5C5B71', fontSize: 15 }}>Year : {value.Year}</Text>
                                <Text style={{ color: '#5C5B71', fontSize: 15 }}>Type : {value.Type}</Text>
                                <Text style={{ color: 'yellow', fontSize: 15 }}> imdbID : {value.imdbID}</Text>
                            </View>
                        </View>
                    ))}

                </ScrollView>

                <View style={styles.footer}>
                    <Icon name='home' size={25} style={{ color: 'white' }} />
                    <Icon name='film' size={25} style={{ color: 'white' }} />
                    <Icon2 name='compass' size={25} style={{ color: 'white' }} />
                    <Icon name='user' size={25} style={{ color: 'white' }} />
                </View>

            </View>
        )
    }
}

export default Assingment1

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#17162E',

    },
    header: {
        width: '100%',
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 15
    },
    footer: {
        width: '100%',
        height: 50,
        backgroundColor: '#1D1C3B',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        marginTop: 15
    },
    search: {
        width: 180,
        height: 40,
        borderRadius: 50,
        backgroundColor: '#1D1C3B',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    box: {
        backgroundColor: '#1D1C3B',
        width: 330,
        height: 180,
        borderRadius: 20,
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignSelf: 'center',
        marginVertical: 10
    },
    poster: {
        width: '30%',
        height: '100%',
        borderRadius: 15,
        borderWidth: 0.5,
    }
})

